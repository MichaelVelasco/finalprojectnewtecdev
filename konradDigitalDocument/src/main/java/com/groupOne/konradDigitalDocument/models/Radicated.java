/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.models;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS RYZEN
 */
@Entity
@Table(name = "radicated")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Radicated.findAll", query = "SELECT r FROM Radicated r"),
    @NamedQuery(name = "Radicated.findById", query = "SELECT r FROM Radicated r WHERE r.id = :id"),
    @NamedQuery(name = "Radicated.findByFilingDate", query = "SELECT r FROM Radicated r WHERE r.filingDate = :filingDate"),
    @NamedQuery(name = "Radicated.findByUserComments", query = "SELECT r FROM Radicated r WHERE r.userComments = :userComments")})
public class Radicated implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "filing_date")
    @Temporal(TemporalType.DATE)
    private Date filingDate;
    @Column(name = "user_comments")
    private String userComments;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "radicated")
//    private Collection<Documents> documentsCollection;
    @JoinColumn(name = "filing_user", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users filingUser;
    @JoinColumn(name = "receptor_user", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Users receptorUser;

    public Radicated() {
    }

    public Radicated(Integer id) {
        this.id = id;
    }

    public Radicated(Integer id, Date filingDate) {
        this.id = id;
        this.filingDate = filingDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(Date filingDate) {
        this.filingDate = filingDate;
    }

    public String getUserComments() {
        return userComments;
    }

    public void setUserComments(String userComments) {
        this.userComments = userComments;
    }

/**    @XmlTransient
    public Collection<Documents> getDocumentsCollection() {
        return documentsCollection;
    }

    public void setDocumentsCollection(Collection<Documents> documentsCollection) {
        this.documentsCollection = documentsCollection;
    } **/

    public Users getFilingUser() {
        return filingUser;
    }

    public void setFilingUser(Users filingUser) {
        this.filingUser = filingUser;
    }

    public Users getReceptorUser() {
        return receptorUser;
    }

    public void setReceptorUser(Users receptorUser) {
        this.receptorUser = receptorUser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Radicated)) {
            return false;
        }
        Radicated other = (Radicated) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.groupOne.konradDigitalDocument.models.Radicated[ id=" + id + " ]";
    }
    
}
