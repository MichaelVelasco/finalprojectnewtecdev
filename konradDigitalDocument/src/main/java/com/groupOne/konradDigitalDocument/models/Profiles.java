/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.models;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ASUS RYZEN
 */
@Entity
@Table(name = "profiles")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profiles.findAll", query = "SELECT p FROM Profiles p"),
    @NamedQuery(name = "Profiles.findById", query = "SELECT p FROM Profiles p WHERE p.id = :id"),
    @NamedQuery(name = "Profiles.findByName", query = "SELECT p FROM Profiles p WHERE p.name = :name")})
public class Profiles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "filingUser")
//    private Collection<Radicated> radicatedCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "receptorUser")
//    private Collection<Radicated> radicatedCollection1;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profile")
//    private Collection<Users> usersCollection;

    public Profiles() {
    }

    public Profiles(Integer id) {
        this.id = id;
    }

    public Profiles(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

/** @XmlTransient
    public Collection<Radicated> getRadicatedCollection() {
        return radicatedCollection;
    }

    public void setRadicatedCollection(Collection<Radicated> radicatedCollection) {
        this.radicatedCollection = radicatedCollection;
    }

    @XmlTransient
    public Collection<Radicated> getRadicatedCollection1() {
        return radicatedCollection1;
    }

    public void setRadicatedCollection1(Collection<Radicated> radicatedCollection1) {
        this.radicatedCollection1 = radicatedCollection1;
    }

    @XmlTransient
    public Collection<Users> getUsersCollection() {
        return usersCollection;
    }

    public void setUsersCollection(Collection<Users> usersCollection) {
        this.usersCollection = usersCollection;
    } **/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profiles)) {
            return false;
        }
        Profiles other = (Profiles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.groupOne.konradDigitalDocument.models.Profiles[ id=" + id + " ]";
    }
    
}
