/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.services;

import com.groupOne.konradDigitalDocument.EmailService;
import com.groupOne.konradDigitalDocument.models.Profiles;
import com.groupOne.konradDigitalDocument.models.Users;
import com.groupOne.konradDigitalDocument.repositories.UsersRepository;
import java.util.Base64;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS RYZEN
 */
@Service
public class UsersService {
    
    @Autowired
    private UsersRepository usersRepository;
    
    public Users createUser(String name, int area, Profiles profile, String email, String password){
        try {
            Users user = new Users();
            user.setName(name);
            user.setArea(area);
            user.setProfile (profile);
            user.setEmail(email);
            user.setPassword(Base64.getEncoder().encodeToString(password.getBytes()));
            this.usersRepository.save(user);
            this.welcomeMessage(user.getEmail());
            return user;
        } catch (Exception e) {
            return null;
        }
    }
    
    public Iterable<Users> readAll(){
        return this.usersRepository.findAll();
    }
            
    public Users readUser(int id){
        if(this.usersRepository.existsById(id)){
            return this.usersRepository.findById(id).get();
        }
        return null;
    }
    
    public Users updateUser(int id, String name, int area, Profiles profile, String email, String password){
        if(this.usersRepository.existsById(id)){
            try {
                Users user = this.usersRepository.findById(id).get();
                user.setName(name);
                user.setArea(area);
                user.setProfile(profile);
                user.setEmail(email);
                user.setPassword(password);
                return this.usersRepository.save(user);
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
    
    public Users deleteUser(int id){
        if(this.usersRepository.existsById(id)){
            try {
                Users user = this.readUser(id);
                this.usersRepository.deleteById(id);
                return user;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
    
    public Users login(String email, String password){
        return this.usersRepository.findByNameAndPassword(email, Base64.getEncoder().encodeToString(password.getBytes()));
    }
    
    
    public void welcomeMessage(String email){
        EmailService emialService = new EmailService();
        emialService.setTo(email);
        emialService.setSubject("Bienvendio!");
        emialService.setMessage("Gracias por registrarse en nuestro proyectyo final.");
        emialService.sendEmail();
    }
}
