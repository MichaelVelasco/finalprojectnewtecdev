/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.services;

import com.groupOne.konradDigitalDocument.models.Documents;
import com.groupOne.konradDigitalDocument.models.Radicated;
import com.groupOne.konradDigitalDocument.models.Users;
import com.groupOne.konradDigitalDocument.repositories.DocumentsRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS RYZEN
 */
@Service
public class DocumentsService {
    
    @Autowired
    private DocumentsRepository documentsRepository;
    
    public Documents createDocument(String title, Date documentDate, Users originUser, Radicated radicated){
        try {
            Documents document = new Documents();
            document.setTitle(title);
            document.setDocumentDate(documentDate);
            document.setOriginUser(originUser);
            document.setRadicated(radicated);
            this.documentsRepository.save(document);
            return document;
        } catch (Exception e) {
            return null;
        }
    }
    
    public Iterable<Documents> readAll(){
        return this.documentsRepository.findAll();
    }
    
    public Documents readDocument(int id){
        if(this.documentsRepository.existsById(id)){
            return this.documentsRepository.findById(id).get();
        }
        return null;
    }
    
    public Documents updateDocument(int id, String title, Date documentDate, Users originUser, Radicated radicated){
        if(this.documentsRepository.existsById(id)){
            Documents document = this.documentsRepository.findById(id).get();
            document.setTitle(title);
            document.setDocumentDate(documentDate);
            document.setOriginUser(originUser);
            document.setRadicated(radicated);
            return this.documentsRepository.save(document);
        }
        return null;
    }
    
    public Documents deleteDocument(int id){
        if(this.documentsRepository.existsById(id)){
            try {
                Documents document = this.readDocument(id);
                this.documentsRepository.deleteById(id);
                return document;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
    
}
