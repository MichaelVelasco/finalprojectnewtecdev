/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.services;

import com.groupOne.konradDigitalDocument.models.Users;
import com.groupOne.konradDigitalDocument.models.Radicated;
import com.groupOne.konradDigitalDocument.repositories.RadicatedRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS RYZEN
 */
@Service
public class RadicatedService {
    
    @Autowired
    private RadicatedRepository radicatedRepository;
    
    public Radicated createRadicated(Users filingUser, Users receptorUser, Date filingDate, String userComments){
        try {
            Radicated radicated = new Radicated();
            radicated.setFilingUser(filingUser);
            radicated.setReceptorUser(receptorUser);
            radicated.setFilingDate(filingDate);
            radicated.setUserComments(userComments);
            this.radicatedRepository.save(radicated);
            return radicated;
        } catch (Exception e) {
            return null;
        }
    }
    
    public Iterable<Radicated> readAll(){
        return this.radicatedRepository.findAll();
    }
    
    public Radicated readRadicated(int id){
        if(this.radicatedRepository.existsById(id)){
            return this.radicatedRepository.findById(id).get(); 
        }
        return null;
    }
    
    public Radicated updateRadicated(int id, Users filingUser, Users receptorUser, Date filingDate, String userComments){
        if(this.radicatedRepository.existsById(id)){
            Radicated radicated = this.radicatedRepository.findById(id).get();
            radicated.setFilingUser(filingUser);
            radicated.setReceptorUser(receptorUser);
            radicated.setFilingDate(filingDate);
            radicated.setUserComments(userComments);
            return this.radicatedRepository.save(radicated);
        }
        return null;
    }
    
    public Radicated deleteRadicated(int id){
        if(this.radicatedRepository.existsById(id)){
            try {
                Radicated radicated = this.readRadicated(id);
                this.radicatedRepository.deleteById(id);
                return radicated;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
    
}
