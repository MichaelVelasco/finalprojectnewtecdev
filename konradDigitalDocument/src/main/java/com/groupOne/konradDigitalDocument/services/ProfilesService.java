/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.services;

import com.groupOne.konradDigitalDocument.models.Profiles;
import com.groupOne.konradDigitalDocument.repositories.ProfilesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS RYZEN
 */
@Service
public class ProfilesService {
    
    @Autowired
    private ProfilesRepository profilesRepository;
    
    public Profiles createProfile(String name){
        try {
            Profiles profile = new Profiles();
            profile.setName(name);
            this.profilesRepository.save(profile);
            return profile;
        } catch (Exception e) {
            return null;
        }
    }
    
    public Iterable<Profiles> readAll(){
        return this.profilesRepository.findAll();
    }
    
    public Profiles readProfile(int id){
        if(profilesRepository.existsById(id)){
            return this.profilesRepository.findById(id).get();
        }
        return null;
    }
    
    public Profiles updateProfile(int id, String name){
        if(profilesRepository.existsById(id)){
            try {
                Profiles profile = this.profilesRepository.findById(id).get();
                profile.setName(name);
                return this.profilesRepository.save(profile);
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
    
    public Profiles deleteProfile(int id){
        if(profilesRepository.existsById(id)){
            try {
                Profiles profile = this.readProfile(id);
                this.profilesRepository.deleteById(id);
                return profile;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
    
}
