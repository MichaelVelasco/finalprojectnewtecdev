/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.controllers;

import com.groupOne.konradDigitalDocument.ResponseHandler;
import com.groupOne.konradDigitalDocument.models.Radicated;
import com.groupOne.konradDigitalDocument.models.Users;
import com.groupOne.konradDigitalDocument.services.DocumentsService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.groupOne.konradDigitalDocument.models.Documents;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author ASUS RYZEN
 */
@RestController
@RequestMapping(path = "documents")
public class DocumentsController {
    
    @Autowired
    private DocumentsService documentsService;
    
    @PostMapping(path = "create")
    public @ResponseBody ResponseEntity createDocument(@RequestParam String title, @RequestParam Date documentDate, @RequestParam Users originUser, @RequestParam Radicated radicated){
        Documents document = this.documentsService.createDocument(title, documentDate, originUser, radicated);
        if(document == null){
            return ResponseHandler.responseHandler("El documento no pudo ser creado.", HttpStatus.PRECONDITION_FAILED, document);
        }
        return ResponseHandler.responseHandler("Documento creado", HttpStatus.OK, document);
    }
    
    @GetMapping(path = "read")
    public @ResponseBody ResponseEntity readDocument(){
        return ResponseHandler.responseHandler("Documentos", HttpStatus.OK, this.documentsService.readAll());
    }
    
    @GetMapping(path = "read/{id}")
    public @ResponseBody ResponseEntity readDocument(@PathVariable int id){
        Documents document = this.documentsService.readDocument(id);
        if (document == null) {
            return ResponseHandler.responseHandler("Documento no encontrado", HttpStatus.NOT_FOUND, document);
        }
        return ResponseHandler.responseHandler("Documento encontrado", HttpStatus.OK, document);
    }
    
    @PutMapping(path = "update/{id}")
    public @ResponseBody ResponseEntity updateDocument(@PathVariable int id, @RequestParam String title, @RequestParam Date documentDate, @RequestParam Users originUser, @RequestParam Radicated radicated){
        Documents document =  this.documentsService.updateDocument(id, title, documentDate, originUser, radicated);
        if(document == null){
            return ResponseHandler.responseHandler("No fue posible editar el docuemnto", HttpStatus.PRECONDITION_FAILED, document);
        }
        return ResponseHandler.responseHandler("Docuemnto editado.", HttpStatus.OK, document);
    }
    
    @DeleteMapping(path = "delete/{id}")
    public @ResponseBody ResponseEntity deleteDocument(@PathVariable int id){
        Documents document = this.documentsService.deleteDocument(id);
        if(document == null){
            return ResponseHandler.responseHandler("No se pudo eliminar el Documento", HttpStatus.PRECONDITION_FAILED, document);
        }
       return ResponseHandler.responseHandler("Documento eliminado.", HttpStatus.OK, document);
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handle(Exception ex) {
        return ResponseHandler.responseHandler(ex.getMessage(), HttpStatus.PRECONDITION_FAILED, null);
    }
    
}
