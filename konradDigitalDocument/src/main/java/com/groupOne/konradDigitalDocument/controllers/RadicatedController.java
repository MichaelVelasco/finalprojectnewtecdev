/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.controllers;

import com.groupOne.konradDigitalDocument.ResponseHandler;
import com.groupOne.konradDigitalDocument.models.Users;
import com.groupOne.konradDigitalDocument.models.Radicated;
import com.groupOne.konradDigitalDocument.services.RadicatedService;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ASUS RYZEN
 */
@RestController
@RequestMapping(path = "radicated")
public class RadicatedController {
    
    @Autowired
    private RadicatedService radicatedService;
    
    @PostMapping(path = "create")
    public @ResponseBody ResponseEntity createRadicated(@RequestParam Users filingUser, @RequestParam Users receptorUser, @RequestParam Date filingDate, @RequestParam String userComments){
        Radicated radicated = this.radicatedService.createRadicated(filingUser, receptorUser, filingDate, userComments);
        if(radicated == null){
            return ResponseHandler.responseHandler("El radicado no pudo ser creado.", HttpStatus.PRECONDITION_FAILED, radicated);
        }
        return ResponseHandler.responseHandler("Radicado creado", HttpStatus.OK, radicated);
    }
    
    @GetMapping(path = "read")
    public @ResponseBody ResponseEntity readRadicated(){
        return ResponseHandler.responseHandler("Radicados", HttpStatus.OK, this.radicatedService.readAll());
    }
    
    @GetMapping(path = "read/{id}")
    public @ResponseBody ResponseEntity readRadicated(@PathVariable int id){
        Radicated radicated = this.radicatedService.readRadicated(id);
        if(radicated == null){
            return ResponseHandler.responseHandler("Radicado no encontrado.", HttpStatus.NOT_FOUND, radicated);
        }
        return ResponseHandler.responseHandler("Radicado encontrado.", HttpStatus.OK, radicated);
    }
    
    @PutMapping(path = "update/{id}")
    public @ResponseBody ResponseEntity updateRadicated(@PathVariable int id, @RequestParam Users filingUser, @RequestParam Users receptorUser, @RequestParam Date filingDate, @RequestParam String userComments){
        Radicated radicated = this.radicatedService.updateRadicated(id, filingUser, receptorUser, filingDate, userComments);
        if(radicated == null){
            return ResponseHandler.responseHandler("El radicado no fue editado.", HttpStatus.PRECONDITION_FAILED, radicated);
        }
        return ResponseHandler.responseHandler("Radicado editado", HttpStatus.OK, radicated);
    }
    
    @DeleteMapping(path = "delete/{id}")
    public @ResponseBody ResponseEntity deleteRadicated(@PathVariable int id){
        Radicated radicated = this.radicatedService.deleteRadicated(id);
        if(radicated == null){
            return ResponseHandler.responseHandler("El radicado no fue eliminado", HttpStatus.PRECONDITION_FAILED, radicated);
        }
        return ResponseHandler.responseHandler("Radicado eliminado", HttpStatus.OK, radicated);
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handle(Exception ex) {
        return ResponseHandler.responseHandler(ex.getMessage(), HttpStatus.PRECONDITION_FAILED, null);
    }
    
    
}
