/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.controllers;

import com.groupOne.konradDigitalDocument.ResponseHandler;
import com.groupOne.konradDigitalDocument.models.Profiles;
import com.groupOne.konradDigitalDocument.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.groupOne.konradDigitalDocument.models.Users;
import java.util.Base64;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 *
 * @author ASUS RYZEN
 */
@RestController
@RequestMapping(path = "users")
public class UsersController {
    
    @Autowired
    private UsersService usersService;
    
    @PostMapping(path = "create")
    public @ResponseBody ResponseEntity createUser(@RequestParam String name, @RequestParam int area, @RequestParam Profiles profile, @RequestParam String email, @RequestParam String password){
        if(password.length() <= 8){
            return ResponseHandler.responseHandler("La contraseña debe ser contener al menos 8 caracteres.", HttpStatus.CONFLICT, null);
        }
        if(!password.matches(".*[A-Z].*")){
            return ResponseHandler.responseHandler("La contraseña debe contener al menos una letra en mayúsculas.", HttpStatus.CONFLICT, null);
        }
        if(!password.matches(".*[0-9].*")){
            return ResponseHandler.responseHandler("La contraseña debe contener al menos un número.", HttpStatus.CONFLICT, null);
        }
        Users user = this.usersService.createUser(name, area, profile, email, password);
        if(user == null){
            return ResponseHandler.responseHandler("No se pudo crear el usuario, valide que el correo sea único.", HttpStatus.PRECONDITION_FAILED, user);
        }
        return ResponseHandler.responseHandler("Usuario creado.", HttpStatus.OK, user);
    }
    
    @GetMapping(path = "read")
    public @ResponseBody ResponseEntity readUser(){
        return ResponseHandler.responseHandler("Usuarios", HttpStatus.OK, this.usersService.readAll());
    }
    
    @GetMapping(path = "read/{id}")
    public @ResponseBody ResponseEntity readUser(@PathVariable int id){
        Users user = this.usersService.readUser(id);
        if(user == null){
            return ResponseHandler.responseHandler("Usuario no encontrado.", HttpStatus.NOT_FOUND, user);
        }
        return ResponseHandler.responseHandler("Usuario encontrado.", HttpStatus.OK, user);
    }
    
    @PutMapping(path = "update/{id}")
    public @ResponseBody ResponseEntity updateUser(@PathVariable int id, @RequestParam String name, @RequestParam int area, @RequestParam Profiles profile, @RequestParam String email, @RequestParam String password){
        Users user = this.usersService.updateUser(id, name, area, profile, email, password);
        if(user == null){
            return ResponseHandler.responseHandler("No se pudo editar el usuario, por favor valide que usuario realmente exista, que el Email sea único y que exista el perfil.", HttpStatus.PRECONDITION_FAILED, user);
        }
        return ResponseHandler.responseHandler("Usuario editado.", HttpStatus.OK, user);
    }
    
    @DeleteMapping(path = "delete/{id}")
    public @ResponseBody ResponseEntity deleteUser(@PathVariable int id){
        Users user = this.usersService.deleteUser(id);
        if(user == null){
            return ResponseHandler.responseHandler("No se pudo eliminar el usuario.", HttpStatus.PRECONDITION_FAILED, user);
        }
        return ResponseHandler.responseHandler("Usuario eliminado", HttpStatus.OK, user);
    }
    
    @GetMapping(path = "login")
    public @ResponseBody ResponseEntity login(@RequestParam String email, @RequestParam String password){
        Users user = this.usersService.login(email, password);
        if(user == null){
            return ResponseHandler.responseHandler("Usuario o contrasña no validos", HttpStatus.PRECONDITION_FAILED, user);
        }
        return ResponseHandler.responseHandler("Login exitoso", HttpStatus.OK, user);
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity handle(Exception ex) {
        return ResponseHandler.responseHandler(ex.getMessage(), HttpStatus.PRECONDITION_FAILED, null);
    }
    
}
