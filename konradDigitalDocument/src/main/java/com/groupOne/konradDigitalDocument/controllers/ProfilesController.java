/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.controllers;

import com.groupOne.konradDigitalDocument.ResponseHandler;
import com.groupOne.konradDigitalDocument.services.ProfilesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.groupOne.konradDigitalDocument.models.Profiles;

/**
 *
 * @author ASUS RYZEN
 */
@RestController
@RequestMapping(path = "profiles")
public class ProfilesController {
    
    @Autowired
    private ProfilesService profilesService;
    
    @PostMapping(path = "create")
    public @ResponseBody ResponseEntity createProfile(@RequestParam String name){
        Profiles profile = this.profilesService.createProfile(name);
        if(profile == null){
            return ResponseHandler.responseHandler("El perfil no se pudo crear.", HttpStatus.PRECONDITION_FAILED, profile);
        }
        return ResponseHandler.responseHandler("Perfil creado.", HttpStatus.OK, profile);
    }
    
    @GetMapping(path = "read")
    public @ResponseBody ResponseEntity readProfile(){
        return ResponseHandler.responseHandler("Perfiles.", HttpStatus.OK, this.profilesService.readAll());
    }
    
    @GetMapping(path = "read/{id}")
    public @ResponseBody ResponseEntity readProfile(@PathVariable int id){
        Profiles profile = this.profilesService.readProfile(id);
        if(profile == null){
            return ResponseHandler.responseHandler("Perfil no encontrado.", HttpStatus.NOT_FOUND, null);
        }
        return ResponseHandler.responseHandler("Perfil encontrado.", HttpStatus.OK, profile);
    }
    
    @PutMapping(path = "update/{id}")
    public @ResponseBody ResponseEntity updateProfile(@PathVariable int id, @RequestParam String name){
        Profiles profile = this.profilesService.updateProfile(id, name);
        if(profile == null){
            return ResponseHandler.responseHandler("El perfil no se pudo editar, por favor valide que el perfil exista realmente.", HttpStatus.PRECONDITION_FAILED, profile);
        }
        return ResponseHandler.responseHandler("Perfil editado.", HttpStatus.OK, profile);
    }
    
    @DeleteMapping(path = "delete/{id}")
    public @ResponseBody ResponseEntity deleteProfile(@PathVariable int id){
        Profiles profile = this.profilesService.deleteProfile(id);
        if(profile == null){
            return ResponseHandler.responseHandler("El perfil no se pudo eliminar, por favor valide que el perfil exista realmente y que no esté asignado a ningún usuario.", HttpStatus.PRECONDITION_FAILED, profile);
        }
        return ResponseHandler.responseHandler("Perfil eliminado.", HttpStatus.OK, profile);
    }
    
}
