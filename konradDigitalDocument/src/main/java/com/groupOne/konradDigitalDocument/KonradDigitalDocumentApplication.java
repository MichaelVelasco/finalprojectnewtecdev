package com.groupOne.konradDigitalDocument;

import java.time.LocalDateTime;
import javax.servlet.http.HttpServletResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import com.fasterxml.jackson.databind.util.JSONPObject;

@SpringBootApplication
public class KonradDigitalDocumentApplication {

    public static void main(String[] args) {
        Class[] classes = {
            KonradDigitalDocumentApplication.class,
            com.groupOne.konradDigitalDocument.controllers.DocumentsController.class,
            com.groupOne.konradDigitalDocument.controllers.ProfilesController.class,
            com.groupOne.konradDigitalDocument.controllers.RadicatedController.class,
            com.groupOne.konradDigitalDocument.controllers.UsersController.class,
        };
        SpringApplication.run(classes, args);
    }
 
    @Configuration
    public class SecurityConfig extends WebSecurityConfigurerAdapter
    {
        @Override
        protected void configure(HttpSecurity http) throws Exception 
        {
            http.csrf().disable().authorizeRequests().anyRequest().authenticated().and()
                .httpBasic().authenticationEntryPoint((request, response, e)-> {
                    response.setContentType("application/json;charset=UTF-8");
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    response.getWriter().write("{\"Unauthorized\":true}");
                });
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
            auth.inMemoryAuthentication()
            .withUser("admin")
            .password("{noop}password")
            .roles("USER");
        }
    }
}
