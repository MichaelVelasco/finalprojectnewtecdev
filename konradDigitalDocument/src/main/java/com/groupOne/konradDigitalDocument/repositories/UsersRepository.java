/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument.repositories;

import com.groupOne.konradDigitalDocument.models.Users;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ASUS RYZEN
 */
@Repository
public interface UsersRepository extends CrudRepository<Users, Integer>{
    Users findByNameAndPassword(String email, String password);
}
