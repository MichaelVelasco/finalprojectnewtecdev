/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument;

import com.groupOne.konradDigitalDocument.models.Profiles;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author maico
 */
public class ResponseHandler implements Serializable{

    public static ResponseEntity<Object> responseHandler(String message, HttpStatus status, Object responseObj) {
        Map<String, Object> map = new HashMap();
        map.put("message", message);
        map.put("status", status.value());
        map.put("data", responseObj);
        return new ResponseEntity(map,status);
    }
    
}
