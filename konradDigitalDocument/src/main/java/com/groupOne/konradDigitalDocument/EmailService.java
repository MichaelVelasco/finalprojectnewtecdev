/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.groupOne.konradDigitalDocument;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author maico
 */
public class EmailService {
    
    private String to = "FinalProjectNewTecDev@gmail.com";
    private String cc = "FinalProjectNewTecDev@gmail.com";
    private String from = "FinalProjectNewTecDev@gmail.com";
    
    private String message = "";
    private String subject = "";
    
    private final String host = "smtp.gmail.com";
    private final String port = "465";
    
    private Session session;
    
    Properties properties = System.getProperties();

    public EmailService() {
        this.setProperties();
        this.setSession();
    }
    
    private void setProperties(){
        properties.put("mail.smtp.host", this.host);
        properties.put("mail.smtp.port", this.port);
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");
    }
    
    private void setSession(){
        this.session = Session.getInstance(this.properties, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("FinalProjectNewTecDev@gmail.com", "Pass123*");
            }
        });
        this.session.setDebug(true);
    }
    
    public void sendEmail(){
    
        try {
            // Create a default MimeMessage object.
            MimeMessage emailMessage = new MimeMessage(this.session);

            // Set From: header field of the header.
            emailMessage.setFrom(new InternetAddress(this.from));

            // Set To: header field of the header.
            emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.to));

            // Set Subject: header field
            emailMessage.setSubject(this.subject);

            // Now set the actual message
            emailMessage.setText(this.message);

            // Send message
            Transport.send(emailMessage);
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
                
    }
    
    public void setTo(String emailTo){
        this.to = emailTo;
    }
    
    public void setCC(String emailCC){
        this.cc = emailCC;
    }
    
    public void setMessage(String message){
        this.message = message;
    }
    
    public void setSubject(String subject){
        this.subject = subject;
    }
}
